require('dotenv').config()
const express = require("express");
const app = express();
const morgan = require('morgan');
const db = require("./db");

const cors = require("cors");

app.use(cors());
app.use(express.json());

// get beaches
app.get("/api/v1/beaches", async (req, res) => {

    try {
        const results = await db.query(
            "select * from plages left join (select plage_id, COUNT(*), TRUNC(AVG(rating),1) as average_rating from reviews group by plage_id) reviews on plages.id = reviews.plage_id"
        );
        
        console.log(results);

        res.status(200).json({
            status: "success getAll",
            results: results.rows.length,
            data: {
                beaches: results.rows,
            },
        });
    } catch (err) {
        console.log(err);
    }
});

// get a beach
app.get("/api/v1/beaches/:id", async (req, res) => {
    console.log(req.params.id);

    try {
        const plage = await db.query("select * from plages left join (select plage_id, COUNT(*), TRUNC(AVG(rating),1) as average_rating from reviews group by plage_id) reviews on plages.id = reviews.plage_id where id = $1",
        [req.params.id]);

        const reviews = await db.query("select * from reviews where plage_id = $1", 
        [req.params.id]);

        res.status(200).json({
            status: "success getId",
            data: {
            beaches: plage.rows[0],
            reviews: reviews.rows,
            },
                });
    } catch (err) {
        console.log(err);
    }
});

// create a beach
app.post("/api/v1/beaches", async (req, res) => {
    console.log(req.body);

    try {
        const results = await db.query("INSERT INTO plages (name, location, quality_range) values ($1, $2, $3) returning *", 
        [req.body.name, req.body.location, req.body.quality_range])
        res.status(200).json({
            status: "success post",
            data: {
            beaches: results.rows[0],
            },
                });
    } catch (err) {
        console.log(err);
    }


});

// update a beach
app.put("/api/v1/beaches/:id", async (req, res) => {

    try {
        const results = await db.query("UPDATE plages SET name = $1, location = $2, quality_range = $3 WHERE id= $4 returning *", 
        [req.body.name, req.body.location, req.body.quality_range, req.params.id])
        res.status(200).json({
            status: "success update",
            data: {
            beaches: results.rows[0],
            },
                });
    } catch (err) {
        console.log(err)
    }
    console.log(req.params.id);
    console.log(req.body);


});

// delete a beach
app.delete("/api/v1/beaches/:id", async (req, res) => {
    console.log(req.params.id);
    console.log(req.body);

    try {
        const results = await db.query("DELETE FROM plages WHERE id = $1 returning *", 
        [req.params.id])
        res.status(200).json({
            status: "success delete",
            data: {
            beaches: results.rows[0],
            },
                });
    } catch (err) {
        console.log(err)
    }
});

// get reviews
app.get("/api/v1/reviews", async (req, res) => {

    try {
        const results = await db.query(
            "select * from reviews"
        );
        
        console.log(results);

        res.status(200).json({
            status: "success getAll",
            results: results.rows.length,
            data: {
                beaches: results.rows,
            },
        });
    } catch (err) {
        console.log(err);
    }
});

// Add a review

app.post("/api/v1/beaches/:id/addReview", async (req, res) => {
    try {
      const newReview = await db.query(
        "INSERT INTO reviews (plage_id, name, review, rating) values ($1, $2, $3, $4) returning *;",
        [req.params.id, req.body.name, req.body.review, req.body.rating]
      );
      console.log(newReview);
      res.status(201).json({
        status: "success add review",
        data: {
          review: newReview.rows[0],
        },
      });
    } catch (err) {
      console.log(err);
    }
  });

  // delete a review
app.delete("/api/v1/beaches/:id/deleteReview", async (req, res) => {
    console.log(req.params.id);
    console.log(req.body);

    try {
        const results = await db.query("DELETE FROM reviews WHERE id = $1 returning *", 
        [req.params.id])
        res.status(200).json({
            status: "success delete",
            data: {
            beaches: results.rows[0],
            },
                });
    } catch (err) {
        console.log(err)
    }
});


app.get("/", (req, res) => {
    res.send("Salut")
})

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`server is up and listening on port ${PORT}`)
});